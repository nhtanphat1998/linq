﻿// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;

var productsJson = File.ReadAllText("products.json");
var categoriesJson = File.ReadAllText("categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);


//Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.

//Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

//Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

//Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.

//Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.

//Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần.

//Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.

//Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó.


//Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

//Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.

//Tính tổng số tiền của tất cả các sản phẩm.

//Lấy danh sách các danh mục và số lượng sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo số lượng sản phẩm giảm dần.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị giảm dần.

//Lấy danh sách các danh mục có ít sản phẩm nhất.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.

//Tính tổng số tiền của các sản phẩm có giá trị lớn hơn $50.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.


class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CategoryId { get; set; }
    public decimal Price { get; set; }
}

class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
}
