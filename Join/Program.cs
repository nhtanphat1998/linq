﻿List<Department> departments = new List<Department>
{
    new Department { Id = 1, Name = "Sales" },
    new Department { Id = 2, Name = "Marketing" },
    new Department { Id = 3, Name = "IT" }
};

List<Employee> employees = new List<Employee>
{
    new Employee { Id = 1, Name = "Alice", DepartmentId = 1 },
    new Employee { Id = 2, Name = "Bob", DepartmentId = 2 },
    new Employee { Id = 3, Name = "Charlie", DepartmentId = 1 },
    new Employee { Id = 4, Name = "David", DepartmentId = 3 }
};

var result = departments.Join(employees,
                              department => department.Id,
                              employee => employee.DepartmentId,
                              (department, employee) => new
                              {
                                  EmployeeName = employee.Name,
                                  DepartmentName = department.Name
                              });

foreach (var item in result)
{
    Console.WriteLine($"{item.EmployeeName} works in {item.DepartmentName} department.");
}
public class Department
{
    public int Id { get; set; }
    public string Name { get; set; }
}

public class Employee
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int DepartmentId { get; set; }
}